package cl.grebolledoa.myapplication.adapters;

import android.media.Image;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import java.util.List;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import cl.grebolledoa.myapplication.R;
import cl.grebolledoa.myapplication.models.Contacto;


public class MyAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Contacto> datos;

    public MyAdapter(Context context, int layout, List<Contacto> datos){
        this.context = context;
        this.layout = layout;
        this.datos = datos;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int position) {
        return datos.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        // Inflar nuestra vista o layout
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        // cargamos la vista que definimos en los layout (list_item.xml)
        view = layoutInflater.inflate(this.layout, null);

        //obtenemos los widgets desde el layout
        TextView tvNombreContacto = view.findViewById(R.id.tv_nombre_contacto);
        TextView tvNumeroContacto = view.findViewById(R.id.tv_numero_contacto);
        ImageButton btLlamar = view.findViewById(R.id.bt_llamar);

        //obtenemos el valor de un dato particular
        Contacto dato = this.datos.get(position);

        //cambialos los datos de nuestros widgets
        tvNombreContacto.setText(dato.getNombre());
        tvNumeroContacto.setText(dato.getNumero());

        btLlamar.setOnClickListener(new View.OnClickListener(){
            @Override

            public void onClick(View v){
                Toast.makeText(context, dato.getNombre(), Toast.LENGTH_LONG).show();
            }
        });

        //retornamos la view con la que trabajamos
        return view;
    }
}
