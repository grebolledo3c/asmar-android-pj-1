package cl.grebolledoa.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.Toast;

public class InformationActivity extends AppCompatActivity {

    private TextView tvMensaje;
    private EditText etNumeroTelefono;
    private ImageButton ibLlamar;
    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        this.tvMensaje = findViewById(R.id.tv_mensaje);
        this.ibLlamar = findViewById(R.id.ib_llamar);
        this.etNumeroTelefono = findViewById(R.id.et_numero_telefono);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            String mensaje = bundle.getString("saludo");
            int numero = bundle.getInt("numero");
            mensaje += " " + numero;
            this.tvMensaje.setText(mensaje);
        }

        this.ibLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numeroTelefono = etNumeroTelefono.getText().toString();

                if(! numeroTelefono.isEmpty()){
                    // comprobamos si ha aceptado o no el permiso
                    if(validarPermisos(Manifest.permission.CALL_PHONE)){
                        // otra forma de preguntar si el permiso fue dado o no
                        //if (ActivityCompat.checkSelfPermission(InformationActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                        Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numeroTelefono));
                        startActivity(i);
                    }else{
                        // se denego el permiso o es la primera vez que se le pregunta
                        if(shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)){
                            Toast.makeText(InformationActivity.this,
                                    "No ha aceptado los permisos", Toast.LENGTH_LONG).show();
                        }else{
                            String[] permisos = new String[]{ Manifest.permission.CALL_PHONE };

                            requestPermissions(permisos,
                                    PHONE_CALL_CODE);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResult){
        if(requestCode == PHONE_CALL_CODE){
            String permiso = permissions[0];
            int resultado = grantResult[0];

            if( permiso.equals(Manifest.permission.CALL_PHONE) ){
                if(resultado == PackageManager.PERMISSION_GRANTED){
                    String numeroTelefono = etNumeroTelefono.getText().toString();
                    Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+numeroTelefono));
                    startActivity(intentCall);
                }else{
                    Toast.makeText(InformationActivity.this,
                            "El usuario no dio permiso", Toast.LENGTH_LONG).show();
                }
            }
        } else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResult);
        }
    }

    private boolean validarPermisos(String permision){
        int resultado = checkCallingOrSelfPermission(permision);
        return (resultado == PackageManager.PERMISSION_GRANTED);
    }
}