package cl.grebolledoa.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.grebolledoa.myapplication.adapters.MyAdapter;
import cl.grebolledoa.myapplication.models.Contacto;

public class GridAgendaActivity extends AppCompatActivity {

    private GridView gvContactos;
    private List<Contacto> contactos = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_agenda);

        this.gvContactos = findViewById(R.id.gv_contactos);

        //TextView tv = findViewById(R.id.tv_mensaje);

        this.contactos.add(new Contacto("Pedro", "+569407065"));
        this.contactos.add(new Contacto("Juan", "+569408060"));
        this.contactos.add(new Contacto("Maria", "+569708064"));
        this.contactos.add(new Contacto("Juana", "+569407090"));
        this.contactos.add(new Contacto("José", "+569205060"));

        MyAdapter adapter = new MyAdapter(this, R.layout.grid_item, contactos);

        this.gvContactos.setAdapter(adapter);



    }
}