package cl.grebolledoa.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.widget.TextView;

import android.widget.EditText;
import android.widget.Button;
import android.view.View;
import android.widget.Toast;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    //private TextView tvMiMensaje;

    private EditText etUsername;
    private EditText etContrasenia;
    private Button btLogin;

    private static final String MASTER_USERNAME = "admin";
    private static final String MASTER_CONTRASENIA = "Asmar.2021";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.etUsername = findViewById(R.id.et_username);
        this.etContrasenia = findViewById(R.id.et_contrasenia);
        this.btLogin = findViewById(R.id.bt_login);

        this.btLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String username = etUsername.getText().toString();
                String contrasenia = etContrasenia.getText().toString();

                if(username.equals(MASTER_USERNAME) && contrasenia.equals(MASTER_CONTRASENIA)){
                    Toast.makeText(getApplicationContext(), "Login exitoso", Toast.LENGTH_LONG).show();
                    /*btLogin.setEnabled(false);
                    etUsername.setText("");
                    etContrasenia.setText("");*/

                    Intent intent = new Intent(MainActivity.this, AgendaActivity.class);
                    // le decimos al intent que el usuario no pueda volver a la pantalla de login
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    String mensaje = "Hola desde el MainActivity";
                    intent.putExtra("saludo", mensaje);
                    intent.putExtra("numero", 123);

                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), "Login erroneo, reintente", Toast.LENGTH_LONG).show();
                }
            };
        });

    }

    /*
        De esta forma podemos llamar el método clickLogin desde nuestro archivo activity_main.xml
        public void clickLogin(View v){
            Toast.makeText(this, "Click", Toast.LENGTH_LONG).show();
        }
    */

    @Override
    protected void onStart() {
        super.onStart();
        //Toast.makeText(this, "OnStart", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "OnResume", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause(){
        super.onPause();
        //Toast.makeText(this, "OnPause", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStop(){
        super.onStop();
        //Toast.makeText(this, "OnStop", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        //Toast.makeText(this, "OnRestart", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        //Toast.makeText(this, "OnDestroy", Toast.LENGTH_LONG).show();
        //Log.i("LIFE CICLE", "OnDestroy");
    }

}