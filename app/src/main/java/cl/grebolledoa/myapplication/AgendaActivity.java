package cl.grebolledoa.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.List;
import java.util.ArrayList;
import android.widget.AdapterView;
import android.view.View;
import android.widget.Toast;

import cl.grebolledoa.myapplication.adapters.MyAdapter;
import cl.grebolledoa.myapplication.models.Contacto;


public class AgendaActivity extends AppCompatActivity {

    private ListView lvContactos;

    //creamos e inicializamos una collecion de string
    private List<Contacto> contactos = new ArrayList<Contacto>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);
        this.lvContactos = findViewById(R.id.lv_contactos);

        //agregamos elementos
        this.contactos.add(new Contacto("Pedro", "+569407065"));
        this.contactos.add(new Contacto("Juan", "+569408060"));
        this.contactos.add(new Contacto("Maria", "+569708064"));
        this.contactos.add(new Contacto("Juana", "+569407090"));
        this.contactos.add(new Contacto("José", "+569205060"));

        //crear un adaptador que nos permite dar forma a como se va ver la listview
        //tres formas de pasar el contexto
        // getAplicationContext()
        // this -> tiene que estar dentro de la clase
        // AgendaActivity.class
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, this.contactos);

        this.lvContactos.setAdapter(myAdapter);

    }
}